#!/bin/bash

setfacl -R -m u:www-data:rwX -m u:"$(whoami)":rwX var
setfacl -dR -m u:www-data:rwX -m u:"$(whoami)":rwX var

if grep -q ^DATABASE_URL= .env; then
  php bin/console doctrine:database:create --if-not-exists

  if [ "$( find ./migrations -iname '*.php' -print -quit )" ]; then
    php bin/console doctrine:migrations:migrate --no-interaction --all-or-nothing
  fi
fi

exec docker-php-entrypoint "$@"
